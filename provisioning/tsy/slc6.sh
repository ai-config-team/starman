#!/bin/bash

# Ignore some packages from EPEL that we do not want.
yum -y install yum-utils
yum-config-manager --save --setopt=epel.exclude=puppet,puppet-server,facter

# Install facter
yum -y install https://straylen.web.cern.ch/straylen/starman/repos/ai6-stable/x86_64/os/facter-2.4.3-1.cern5.ai6.x86_64.rpm

# Install epel-release and puppet
#yum -y install epel-release
yum -y install --enablerepo=epel https://straylen.web.cern.ch/straylen/starman/repos/ai6-stable/x86_64/os/puppet-3.8.7cern1-1.ai6.noarch.rpm

cat > /etc/puppet/puppet.conf << EOF
[main]
vardir                    = /var/lib/puppet
ssldir                    = /var/lib/puppet/ssl
rundir                    = /var/run/puppet
pluginsync                = true

[agent]
server                    = it-puppet-masters-public-a.cern.ch
ca_server                 = funnyca.cern.ch
masterport                = 9170
ca_port                   = 9171
stringify_facts           = false
configtimeout             = 500
report                    = true
environment               = starman
runinterval               = 600
certificate_revocation    = false
dynamicfacts              = memorysize,memoryfree,swapsize,swapfree,uptime_seconds,uptime_hours
ignoreschedules           = true
EOF

#{csr_custom_attributes}

puppet agent -tv --tags osrepos
yum -y reinstall pam openssl
yum distro-sync
/sbin/shutdown -r now
