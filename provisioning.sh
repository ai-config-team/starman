#!/bin/bash

yum -y install http://linuxsoft.cern.ch/repos/config7-stable/x86_64/os/Packages/puppet-agent-1.9.3.13.g95a30cd-64bit.1.ai7.x86_64.rpm

source /etc/profile.d/puppet-agent.sh

mkdir /etc/systemd/system/puppet.service.d

cat > /etc/systemd/system/puppet.service.d/datacentre.conf << EOF
[Service]
Environment=FACTER_datacentre=orabmc
EOF

systemctl daemon-reload

cat > /etc/puppetlabs/puppet/puppet.conf << EOF
[main]
ssldir                    = /var/lib/puppet/ssl

[agent]
server                    = it-puppet-masters-public-a.cern.ch
ca_server                 = sca01.cern.ch
masterport                = 9170
ca_port                   = 9171
report                    = true
environment               = starman
splay                     = true
splaylimit                = 900
listen                    = false
runinterval               = 2600
certificate_revocation    = false
usecacheonfailure         = false
EOF

FACTER_datacentre=orabmc puppet agent -tv --tags=osrepos
FACTER_datacentre=orabmc puppet agent -tv